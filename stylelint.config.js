module.exports = {
    extends: [
        // https://www.npmjs.com/package/stylelint-config-standard
        'stylelint-config-standard',
        'stylelint-config-recommended',
        // https://www.npmjs.com/package/stylelint-config-prettier
        'stylelint-config-prettier',
    ],
};
