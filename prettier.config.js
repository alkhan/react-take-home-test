module.exports = {
    arrowParens: 'always',
    tabWidth: 4,
    proseWrap: 'always',
    singleQuote: true,
    trailingComma: 'all',
    useTabs: false,
    overrides: [
        {
            files: 'package*.json',
            options: {
                printWidth: 1000,
            },
        },
    ],
};
