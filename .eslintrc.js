module.exports = {
    plugins: ['sonarjs'],
    extends: [
        // https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb
        'airbnb',
        'airbnb/hooks',
        // https://www.npmjs.com/package/eslint-plugin-jest
        'plugin:jest/recommended',
        // 'plugin:jest/style',
        // https://www.npmjs.com/package/eslint-plugin-jsx-a11y
        'plugin:jsx-a11y/recommended',
        // https://www.npmjs.com/package/eslint-plugin-import
        'plugin:import/errors',
        'plugin:import/warnings',
        // https://www.npmjs.com/package/eslint-plugin-react
        'plugin:react/recommended',
        // https://www.npmjs.com/package/eslint-plugin-react-hooks
        'plugin:react-hooks/recommended',
        'plugin:sonarjs/recommended',
        // doit être en dernier
        'plugin:prettier/recommended',
    ],
    globals: {
        __DEV__: true,
    },
    env: {
        browser: true,
        es6: true,
        node: true,
        // amd: true,
        jest: true,
        // 'jest/globals': true, // https://www.npmjs.com/package/eslint-plugin-jest#usage
        'shared-node-browser': true,
    },
    overrides: [
        {
            files: ['**/*.test.js'],
            rules: {
                // https://www.npmjs.com/package/eslint-plugin-jest
                'jest/expect-expect': 'error',
                'jest/no-identical-title': 'error',
                'jest/no-jest-import': 'error',
                'jest/no-large-snapshots': ['warn', { maxSize: 300 }],
                'jest/prefer-strict-equal': 'error',
                'jest/prefer-to-be-null': 'error',
                'jest/prefer-to-be-undefined': 'error',
                'jest/prefer-to-have-length': 'error',
                'jest/valid-expect': 'error',
            },
        },
    ],
    rules: {
        'no-console': 'error',
        'no-alert': 'error',
        'no-debugger': 'error',
        'max-len': ['error', { code: 160 }],
        strict: 'error',
        'no-confusing-arrow': ['error', { allowParens: false }],
        'no-underscore-dangle': 0, // e.g. __DEV__
        'sort-keys': 0,
        'function-call-argument-newline': ['error', 'consistent'],
        'function-paren-newline': ['error', 'consistent'],

        // https://github.com/benmosher/eslint-plugin-import
        'import/extensions': 0,
        'import/order': [
            'error',
            {
                warnOnUnassignedImports: true,
                groups: [
                    'builtin',
                    'external',
                    'parent',
                    'sibling',
                    'index',
                    'object',
                    'unknown',
                ],
                // 'newlines-between': 'always',
                // alphabetize: {
                //     order: 'asc',
                //     caseInsensitive: false,
                // },
            },
        ],
        'import/prefer-default-export': 0,

        // https://www.npmjs.com/package/eslint-plugin-react-hooks
        'react-hooks/rules-of-hooks': 'error',
        'react-hooks/exhaustive-deps': 'error',
    },
    settings: {
        'import/resolver': {
            node: {
                extensions: ['.js', '.jsx', '.ts', '.tsx', '.d.ts'],
            },
        },
        react: {
            version: 'detect',
        },
    },
};
