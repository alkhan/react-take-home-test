import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Card from '../Card/Card';

import './Category.scss';

const propTypes = {
    data: PropTypes.shape({
        id: PropTypes.string,
        items: PropTypes.arrayOf(
            PropTypes.shape({
                title: PropTypes.string,
                photoUrL: PropTypes.string,
                id: PropTypes.string,
            }),
        ),
        title: PropTypes.string,
    }).isRequired,
    onSelect: PropTypes.func,
};

const defaultProps = {
    onSelect: () => {},
};

function Category({ data: { items, title }, onSelect }) {
    const [currentSelection, setCurrentSelection] = useState();
    if (items.length === 0) {
        return null;
    }
    const onSelectHandler = (selection) => {
        setCurrentSelection(selection);
        onSelect(title, selection);
    };
    return (
        <div className="category">
            <h2>{title}</h2>
            <div className="card-container">
                {items.map((nominee) => (
                    <Card
                        key={nominee.id}
                        nominee={nominee}
                        onSelect={onSelectHandler}
                        disabled={
                            currentSelection &&
                            nominee.title !== currentSelection
                        }
                    />
                ))}
            </div>
        </div>
    );
}

Category.propTypes = propTypes;
Category.defaultProps = defaultProps;
export default Category;
