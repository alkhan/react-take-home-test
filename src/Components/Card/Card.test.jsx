import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import Card from './Card';

describe('Card', () => {
    function getCardComponent() {
        return (
            <Card
                nominee={{
                    id: 'T1',
                    title: 'my title',
                    photoUrL:
                        'https://variety.com/wp-content/uploads/2020/12/nomadland_ver2.jpg',
                }}
            />
        );
    }
    beforeEach(() => {
        render(getCardComponent());
    });
    test('has a title', () => {
        expect(screen.getByText('my title')).toBeInTheDocument();
    });

    test('has an image', () => {
        expect(screen.getByAltText('my title')).toBeInTheDocument();
    });

    test('has a button', () => {
        expect(screen.getByText('Select')).toBeInTheDocument();
    });

    test('button text changes when clicked', () => {
        fireEvent.click(screen.getByText('Select'));
        expect(screen.getByText('Deselect')).toBeInTheDocument();
    });
});
