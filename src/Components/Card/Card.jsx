import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './Card.scss';

const propTypes = {
    nominee: PropTypes.shape({
        title: PropTypes.string,
        photoUrL: PropTypes.string,
        id: PropTypes.string,
    }).isRequired,
    onSelect: PropTypes.func,
    disabled: PropTypes.bool,
};

const defaultProps = {
    onSelect: () => {},
    disabled: false,
};

function Card({ nominee: { title, photoUrL }, onSelect, disabled }) {
    const [isSelected, setSelected] = useState(false);
    const onClickHandler = () => {
        setSelected(!isSelected);
        onSelect(!isSelected ? title : undefined);
    };
    return (
        <div className={classNames('card', { selected: isSelected })}>
            <h3>{title}</h3>
            <div className="card-content">
                <img src={photoUrL} alt={`${title}`} />
            </div>
            <button type="button" onClick={onClickHandler} disabled={disabled}>
                {isSelected ? 'Deselect' : 'Select'}
            </button>
        </div>
    );
}

Card.propTypes = propTypes;
Card.defaultProps = defaultProps;
export default Card;
