import React from 'react';
import PropTypes from 'prop-types';
import Close from './close.svg';

import './Modal.scss';

const propTypes = {
    children: PropTypes.node.isRequired,
    show: PropTypes.bool,
    handleClose: PropTypes.func,
};

const defaultProps = {
    show: false,
    handleClose: () => {},
};

const Modal = ({ handleClose, show, children }) => {
    const showHideClassName = show
        ? 'modal display-block'
        : 'modal display-none';

    return (
        <div className={showHideClassName}>
            <section className="modal-main">
                <button type="button" className="close" onClick={handleClose}>
                    <img src={Close} alt="close" />
                </button>
                <div>{children}</div>
            </section>
        </div>
    );
};

Modal.propTypes = propTypes;
Modal.defaultProps = defaultProps;
export default Modal;
