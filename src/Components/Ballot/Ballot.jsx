import React, { useEffect, useState } from 'react';
import api from '../../Api/Api';
import Category from '../Category/Category';
import Modal from '../Modal/Modal';

import './Ballot.scss';

function Ballot() {
    const [items, setItems] = useState([]);
    const [selection, setSelection] = useState({});
    const [isShow, setShow] = useState(false);
    useEffect(() => {
        setItems(null);
        api.getBallotData().then((data) => setItems(data.items));
    }, []);

    const onSelectHandler = (category, currentSelection) => {
        const newSelection = { ...selection };
        if (currentSelection) {
            newSelection[category] = currentSelection;
        } else {
            delete newSelection[category];
        }
        setSelection(newSelection);
    };

    function buildCategory() {
        if (!items || items.length === 0) {
            return <div>No data</div>;
        }
        return (
            <div className="ballot-list">
                {items.map((item) => (
                    <Category
                        key={item.id}
                        data={item}
                        onSelect={onSelectHandler}
                    />
                ))}
            </div>
        );
    }

    if (items === null) {
        return <div>loading.....</div>;
    }
    return (
        <>
            <div className="ballot">
                <div className="header">
                    <h1>AWARDS 2021</h1>
                    <button
                        type="button"
                        onClick={() => setShow(true)}
                        disabled={
                            Object.keys(selection).length !== items.length
                        }
                    >
                        Submit ballot
                    </button>
                </div>
                {buildCategory()}
            </div>
            <Modal show={isShow} handleClose={() => setShow(false)}>
                <ul>
                    {Object.keys(selection).map((category) => (
                        <li key={category}>
                            {category}: {selection[category]}
                        </li>
                    ))}
                </ul>
            </Modal>
        </>
    );
}

export default Ballot;
